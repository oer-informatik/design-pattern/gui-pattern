# GUI-(Architektur)-Pattern

<span class="hidden-text">
https://oer-informatik.de/gui_pattern_mvc
</span>

> **tl/dr;** _(ca. 12 min Lesezeit): Benutzeroberflächen werden häufig in drei Bestandteile aufgeteilt: die Datenhaltungsschicht (Model), die Geschäftslogik (Controller) und die Presentationsschicht (View). Ausgehend von dieser Dreiteilung wagen wir einen Blick auf das GUI-Pattern MVC, dessen Herkunft, die zentralen Eigenschaften, die alle Interpretationen des Patterns gemein haben. Ist überall MVC drin, wo MVC draufsteht?_


(Dieser Artikel ist Bestandteil einer Serie von Artikeln zu GUI-Pattern, in der [MVC](https://oer-informatik.de/gui_pattern_mvc), [MVVM](https://oer-informatik.de/gui_pattern_mvvm) und [MVP](https://oer-informatik.de/gui_pattern_mvp) voneinander abgegrenzt werden.)

## Gemeinsamkeiten der meisten GUI-Pattern

Mit der Programmiersprache _Smalltalk_ wurde 1979 erstmals ein Architekturmuster eingeführt, das die Geschäftslogik und die Präsentationsschicht als komplett eigenständige Komponenten kapselte. Diese Trennung folgt dem Designprinzip _"Seperation of Concerns" (SoC)_. Die Abstraktion der Datenstruktur der Problemdomäne (Model) wird getrennt von der Präsentationsschicht (_View_ + _Controller_-Paar). Martin Fowler^[https://martinfowler.com/eaaDev/uiArchs.html#ModelViewController] spricht hier von _Separated Presentation_ und zieht parallelen dieser Entwicklung etwa zu den UNIX-Befehlen, für die es die _Separated Presentations_ Kommandozeile (_command line interface_, CLI) und graphische Oberfläche (_graphical user interface_, GUI) gibt.

![](plantuml/MVC-Class-small.png)

Heutige GUI-Pattern bauen häufig auf dem bei Smalltalk eingeführten _Model-View-Controller_ (MVC)-Muster auf. Dieses kennt in der ursprünglichen Version drei Rollen: das _Model_ für die Daten/Persistenz, der _Controller_ für die Business-Logik und die _View_ für die eigentliche Darstellung.

Die Aufteilung in drei Komponenten hat sich in den verschiedenen Spielarten des MVC-Musters erhalten, jedoch gibt es in den konkreten Ausprägungen dieser GUI-Muster in verschiedenen Frameworks große Unterschiede:

- Die **Granularität**: gibt es für eine "Seite" jeweils  _Model_, _View_ und _Controller_ oder für jede Seitenkomponente (z.B. Textbox)?

- Die **Verantwortlichkeiten**: Welche Aufgaben sind in den jeweiligen Rollen enthalten? Wer ist z.B. für Validierung und für die Geschäftslogik verantwortlich?

- Die **Abhängigkeiten**: Wie sind die Beziehungen der drei Rollen zueinander: wer hält Referenzen auf welche andere Rolle?
- Die **Kommunikation**: Wer benachrichtigt wen bei Aktualisierungen? Wer verarbeitet die Anfragen der Benutzer*innen?

## Die Aufteilung in _Model_ / _View_ / _Controller_ (_Presenter_ / _ViewModel_)

### Das Modell (englische Schreibweise: _Model_)

Das _Model_ regelt und kapselt den Zugriff und die Verarbeitung der Daten der Problemdomäne und hält den Zustand, der diese Daten repräsentiert.  Nur der Teil der Geschäftslogik, der direkt mit der Speicherung, Änderung, Löschung oder Erzeugung von Daten zusammenhängt, liegt im Verantwortungsbereich des _Models_, ebenso wie Operationen, die unmittelbar auf dem Model ausgeführt werden müssen (z.B. die Berechnung von abgeleiteten Attributen wie z.B. Durchschnitten oder anderen _derivied values_).

Idealerweise ist jedes _Model_ für sich abgeschlossen und eigenständig: Änderungen eines _Models_ sollten möglichst keine anderen _Model_ betreffen. Das _Model_ liefert auf Anfrage den Zustand der Daten und reagiert auf Befehle diesen zu ändern.

Das _Model_ darf weder vom _Controller_ noch von der _View_ direkt abhängen. _Controller_ verfügen über direkten Zugriff (eine Referenz) auf das _Model_ und aktualisieren dieses.

Die Kommunikation mit der View erfolgt indirekt: Entweder wird über den _Controller_ kommuniziert (z.B. im MVP-Pattern, hier heißt der _Controller_ jedoch _Presenter_ ) oder es wird beispielsweise über das _Observer_-Pattern eine lose Kopplung zwischen _Model_ und _View_ realisiert: _Views_ registrieren sich als _Observer_ bei den _Models_. Im Falle einer Aktualisierung benachrichtigen die _Models_ alle registrierten _Views_ über Änderungen.

Bei dem Modell selbst kann es sich wiederum um mehrere Einzelkomponenten handeln: die Datenstruktur wird häufig mit einfachen Entitäts-Klassen (in Java "_POJOs_": plain old Java objects) abgebildet, für komplexere Geschäftslogik und die Logik zur Datenspeicherung (Repositories und PersistenceUnits) werden gemäß des _Single Responsibility Principles_ weitere Klassen implementiert.

### Die _View_

Die _View_ ist verantwortlich für die Präsentation der Daten eines _Models_. Es ist möglich, dass unterschiedliche _Views_ die selben Daten unterschiedlich darstellen. Das selbe _Model_ kann unter Umständen als Liste, Tabelle, Formular oder Diagramm dargestellt werden oder für unterschiedliche Endgeräte (als Website, DesktopApp oder für andere Geräte).

Zwischen _View_ und _Model_ bestehen keine direkten Abhängigkeiten, sie sind höchstens über Interfaces lose gekoppelt (_Observer_-Pattern). Durch diese Entkopplung wird erreicht, dass die Komponenten leicht testbar, veränderbar (ETC: _easy to change_) und austauschbar sind.

Die unterschiedlichen Realisierungen unterscheiden sich darin, in wie weit in der View bereits Geschäftslogik enthalten ist (z.B. Validierung, Zusammenstellung von AJAX-Requests usw.).

### Der _Controller_

Der _Controller_ kommuniziert sowohl mit dem _Model_ als auch mit der _View_ und stellt das Bindeglied zwischen den beiden dar.

Er nimmt die Anfragen des Benutzers entgegen (teilweise mittelbar über die _View_), gibt Datenänderungen an das Model weiter. Er ruft also unmittelbar die Getter- und Setter-Methoden des _Models_ auf.

Er wählt die korrekte _View_ aus, stellt die nötigen Daten zusammen und sendet alles an den anfragenden Client oder aktualisiert die jeweilige Komponente der _View_.

Jede GUI-Komponente (_Widget_, also jede Textbox, jeder Button usw.) kann über ein eigenes _View/Controller_-Paar verfügen.

![](plantuml/MVC-Class-small2.png)

## Nachrichtenfolge im MVC-Pattern

Ein typischer Nachrichtenablauf wäre etwa der folgende:

![](plantuml/mvc-observer-sequenz.png)

- Ein Benutzer gibt in einer GUI sein Geburtsdatum ein und drückt "Speichern"

- Der _Controller_ erhält die Nachricht mit den neuen Werten und führt die benötigte Logik aus (hier: ruft die Setter-Funktion des _Model_ auf)

- Am _Model_ ist die _View_ über das Observer-Pattern angemeldet. In der Setter-Methode ist festgelegt, dass alle Beobachter informiert werden sollen, sobald ein Wert sich ändert - daher wird die _View_ über neue vorliegende Werte informiert

- Die _View_ holt sich die neuen Werte beim _Model_ ab und stellt sie dar.

## Vorteile und umgesetzte Prinzipien

- _Separation of Concerns_ (SoC): die Schichten View, Controller und Model werden unterschiedlich häufig, aus unterschiedlichen Gründen und ggf. von unterschiedlichen Abteilungen geändert. Eine klare Trennung der _Concerns_ sorgt für verständlichen Code, der nicht mit Bestandteilen mehrerer _Concerns_ verunreinigt ist.

-  Der Code bleibt so _easy to change_ (ETC): die _View_ wird in der Regel häufiger geändert als das _Model_ – oftmals auch durch unterschiedliche Abteilungen. Diese Änderungen wirken sich nur auf definierte Codeabschnitte aus.

-	Der Code ist leichter testbar, da die unterschiedlichen _Concerns_ separat getestet werden können – evtl. Abhängigkeiten können durch die klare Trennung und Vermeidung von direkten Abhängigkeiten über Mocking eingebunden werden.

-	Das Prinzip des DRY wird gewährleistet: Die Codeabschnitte zum _Model_ können ggf. von unterschiedlichen _Views_ wiederverwendet werden.

Jede Komponente auf einer Benutzeroberfläche (ein sogenanntes _Widget_: ein "Window-Gadget") kann über ein eigenes Paar aus _View_ und _Controller_ verfügen - also etwa jede Textbox, jeder Radiobutton usw. Darüber hinaus gibt es oft für die Gesamtheit aller Komponenten ein _View_ /_Controller_-Paar.

Gemeinsam haben die meisten GUI-Pattern die Aufteilung in die drei Bestandteile _Model_, _View_ und _Controller_. Sie unterscheiden sich jedoch stark in den Vorgaben, in welchem Umfang diese Komponenten Geschäftslogik realisieren und wie sie miteinander kommunizieren.

## Die Theorie

Martin Fowler^[https://martinfowler.com/eaaDev/uiArchs.html#ModelViewController] umreist die wesentlichen Eigenschaften von MVC folgendermaßen:

> -	Make a strong separation between presentation (view & controller) and domain (model) - Separated Presentation.
> -	Divide GUI widgets into a controller (for reacting to user stimulus) and view (for displaying the state of the model). Controller and view should (mostly) not communicate directly but through the model.
> -	Have views (and controllers) observe the model to allow multiple widgets to update without needed to communicate directly - Observer Synchronization.


## Probleme bei MVC

Aus den Rollen der MVC-Komponenten ergeben sich einige Sonderfälle, mit denen MVC nur schwer umgehen kann:

### Wohin mit Business-Logik, die zur Darstellung der _View_ nötig ist (Validierung, Internationalisierung usw.)?

Wenn die Darstellung der _View_ in Abhängigkeit von Zuständen des Modells variieren soll (z.B. bei Überschreitung bestimmter Werte) müsste die   _View_ Geschäftslogik enthalten, die eigentlich Bestandteil der Problemdomäne ist. Wo sollte die dann sinnvoll untergebracht werden?

- Im _Controller_ - als Ort für die (übergeordnete) Geschäftslogik,

- im _Model_, da nur dort die benötigten Zustände bekannt sind - und hier die das _Model_ direkt betreffende Geschäftslogik steht,

- oder in der _View_, da es schließlich um Fragen der Darstellung geht?

### Wo speichere ich Zustände der _View_, die nicht Teil der Problemdomäne (also des _Models_) sind?

In manchen Anwendungsfällen ist es wichtig, Komponenten der _View_ einen Zustand zuzusprechen: ausgegraute Buttons, vorausgewählte Einträge, übergeordnete Informationen. Auch hier stellt sich die Frage, ob es Aufgabe der _View_ ist, diesen Zustand vorzuhalten - oder ob es im _Model_ gespeichert werden sollte.

### Lösung mit _Application Model_ / _Presentation Model_

Das Ausgangsproblem war:

1. Zusätzlich zu den Attributen der Problemdomäne (des _Models_) müssen also noch andere (_View_-abhängige) Zustände gespeichert werden.

2. Zusätzlich zur Geschäftslogik der Problemdomäne muss auch die _View_-abhängige Geschäftslogik erfasst werden.

Eine mögliche Lösung ist, zwischen _Model_ und _View_ noch eine Zwischenschicht zu setzt, die den Zugriff auf die Attribute des _Models_ ergänzt um die zusätzliche nötigen _View_-Zustände (Attribute) und Geschäftslogiken (Methoden). Diese Zwischenschicht wird häufig _Application Model_ oder _Presentation Model_ genannt. Sie ist mit der _View_ über das Oberserver-Pattern verknüpft - die Anbindung des eigentlichen _Models_ erfolgt nur mittelbar über diese Zwischenschicht (beispielsweise über das _DesignPattern_ _Adapter_).

## Unterschiedliche Ausprägungen und Weiterentwicklungen von MVC heute

Die ursprüngliche in Smalltalk umgesetzte Idee, dass für jedes _Widget_ einer _Application_ ein eigenes MVC-Triple existiert ist heute in Frameworks nicht mehr anzutreffen. Statt dessen haben sich andere Weiterentwicklungen des MVC-Patterns etabliert:


- [Das Model-View-Controller-Pattern (MVC)](https://oer-informatik.de/gui_pattern_mvc): Der Ursprung aller modernen GUI-Pattern

- [Das Model-View-Presenter-Pattern (MVP)](https://oer-informatik.de/gui_pattern_mvp):  Häufig in AJAX-basierten Frameworks anzutreffendes, komplett entkoppeltes GUI-Pattern

- [Das Model-View-ViewModel-Pattern (MVVM)](https://oer-informatik.de/gui_pattern_mvvm): Realsierung in vielen modernen Desktop-App Frameworks


## Links und weitere Informationen

* [Martin Fowler über die User-Interfave-Pattern](https://martinfowler.com/eaaDev/uiArchs.html#ModelViewController)

## _Quellen und offene Ressourcen (OER)_

Die Ursprungstexte (als Markdown), Grafiken und zugrunde liegende Diagrammquelltexte finden sich (soweit möglich in weiterbearbeitbarer Form) in folgendem git-Repository:

[https://gitlab.com/oer-informatik/design-pattern/gui-pattern](https://gitlab.com/oer-informatik/design-pattern/gui-pattern).

Sofern nicht explizit anderweitig angegeben sind sie zur Nutzung als Open Education Resource (OER) unter Namensnennung (H. Stein, oer-informatik.de) freigegeben gemäß der [Creative Commons Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de).

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/deed.de)
