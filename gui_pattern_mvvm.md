# Model- View - ViewModel (MVVM)

<span class="hidden-text">
https://oer-informatik.de/gui_pattern_mvvm
</span>

> **tl/dr;** _(ca. 4 min Lesezeit): Model-View-Viewmodel ist als weiterer Nachkomme des GUI-Patterns MVC vor allem durch C# XAML und Jaca FXML bekannt geworden. Was sind die zentralen Eigenschaften dieses Patterns und worin unterscheidet es sich vom urspünglichen MVC und MVP?_


(Dieser Artikel ist Bestandteil einer Serie von Artikeln zu GUI-Pattern, in der [MVC](https://oer-informatik.de/gui_pattern_mvc), [MVVM](https://oer-informatik.de/gui_pattern_mvvm) und [MVP](https://oer-informatik.de/gui_pattern_mvp) voneinander abgegrenzt werden.)

## Kernaspekte diese Patterns:

- Trennung von GUI-Gestaltung und Code durch gesonderte IDE-Tools und Auszeichnungssprachen (z.B: Java/FXML oder C#/XAML).

- die View ist zustandslos (FXML / XAML-Datei)

- Nachrichtenfluss zwischen den Komponenten über Databinding (Observer-Pattern, ggf. sogar bidirektional umgesetzt)

- Realisierungen ist z.B. über folgende Frameworks möglich: _Windows Presentation Foundation (WPF)_, _Java FX_, _Angular_, _Vue.js_


![MVVM Klassen und deren Aufgaben mit Bidirektionalem Binding](plantuml/MVVM-Class.png)


## _Model_

Die Rolle des _Model_ ist auch im MVVP-Pattern weitgehend identisch zu der Rolle in MVP und MVC. Das _Model_ hat keinerlei Abhängigkeiten zu den andern beiden Komponenten (_View_ und _ViewModel_). Es besteht jedoch - wie bei MVC - die Möglichkeit, dass die _View_ über das _Observer_-Pattern oder Databinding lose gekoppelt direkt mit dem _Model_ interagiert und Werte aktualisiert.


## _View_

Im _MVVM_-Pattern wird die _View_ mit einer völlig eigenen Auszeichnungssprache (bspw. WPF: XAML, JavaFX: FXML) und eigenen Tools (WPF: Expression Blend , JFX: SceneBuilder) umgesetzt. Häufig enthält die _View_ bereits Logik zur Validierung (sofern sie deklarativ in der Auszeichnungssprache festgelegt werden kann).  Die Realisierung WPF von Microsoft bietet zudem die Möglichkeit, Logik per _CodeBehind_ auszulagern. Da die View deklarativ erstellt wird bleibt sie zustandslos. Die Präsentationslogik selbst verbleibt aber im ViewModel

## _ViewModel_

Das ViewModel bildet den Zustand der View und steuert die Kommunikation zwischen _View_ und _Model_. Kennzeichnend hierfür ist das bidirektionale Binding zwischen Model und ViewModel sowie zwischen ViewModel und View. Das ViewModel selbst enthält jedoch keine Kenntnis über GUI-Elemente der View (abgesehen von den Bindings). Dadurch wird sichergestellt, dass die Präsentationslogik auch ohne eine gestartete GUI getestet werden kann - sie kann folglich bereits mit Unit-Testfällen getestet werden und muss nicht mit den relativ teuren Integrationstests bei laufender graphischer Umgebung getestet werden.

## Bidirektionales Binding

Kennzeichnend für das MVVM-Pattern ist neben der rein deklarativen und zustandslosen View v.a. das Binding zwischen den Komponenten. Es kann sich hierbei um vier miteinander verwobene Observer handeln: Das ViewModel beobachtet View und Model, das Model und die View beobachten jeweils das ViewModel. Beispielhaft ist die Nachrichtenfolge hier einmal dargestellt:

![MVVM-Beispiel mit unidirektionalem Databinding](plantuml/mvvm-sequenz.png)

## Andere GUI-Pattern

Zum besseren Verständnis und den Unterschieden der einzelnen GUI-Pattern sollten auch die Artikel der anderen Pattern gelesen werden:


- [Das Model-View-Controller-Pattern (MVC)](https://oer-informatik.de/gui_pattern_mvc): Der Ursprung aller modernen GUI-Pattern

- [Das Model-View-Presenter-Pattern (MVP)](https://oer-informatik.de/gui_pattern_mvp):  Häufig in AJAX-basierten Frameworks anzutreffendes, komplett entkoppeltes GUI-Pattern

- [Das Model-View-ViewModel-Pattern (MVVM)](https://oer-informatik.de/gui_pattern_mvvm): Realsierung in vielen modernen Desktop-App Frameworks

## Links und weitere Informationen


## _Quellen und offene Ressourcen (OER)_

Die Ursprungstexte (als Markdown), Grafiken und zugrunde liegende Diagrammquelltexte finden sich (soweit möglich in weiterbearbeitbarer Form) in folgendem git-Repository:

[https://gitlab.com/oer-informatik/design-pattern/gui-pattern](https://gitlab.com/oer-informatik/design-pattern/gui-pattern).

Sofern nicht explizit anderweitig angegeben sind sie zur Nutzung als Open Education Resource (OER) unter Namensnennung (H. Stein, oer-informatik.de) freigegeben gemäß der [Creative Commons Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de).

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/deed.de)
